/**
 * There are implementation of Linked Queue
 * based on Single Linked Queue
 * @param <T>
 */
public class LinkedQueue<T> {
    private SingleLinkedList<T> list = new SingleLinkedList<T>(); // Main field - Single Linked List


    /**
     * Add in the end of queue
     * @param value
     */
    public void enqueue(T value) { list.addLast(value); }


    /**
     * Add element in queue
     * @return
     */
    public T getFirst() { return list.getByIndex(0); }


    /**
     * Remove element from queue
     * @return
     */
    public T dequeue() { return list.removeFirst(); }


    /**
     * Get size of queue
     * @return
     */
    public int getSize() { return list.getSize(); }


    /**
     * Print all elements if queue
     */
    public void printQueue() { list.printList(); }

}
