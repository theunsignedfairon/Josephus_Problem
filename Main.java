import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    /**
     * From this method program will start execute
     * @param args
     */
    public static void main(String[] args) throws IOException {
        // Declaration
        Scanner sc = new Scanner(new FileReader("input.txt")); // Scan from file
        PrintWriter wr = new PrintWriter("output.txt"); // Print in file
        LinkedQueue<String> my_queue = new LinkedQueue<String>(); // Object of Linked

        // Read data from file
        String[] input_string_array = sc.nextLine().split(" "); // Split input line by " "
        int k = Integer.parseInt(input_string_array[input_string_array.length - 1]), counter = 0; // K - take last element from "input_string_array" and convert to Integer

        // We add elements in queue in loop
        for (int index = 0; index < input_string_array.length - 1 ; index++) {
            my_queue.enqueue(input_string_array[index]);
        }

        // Main loop
        while (my_queue.getSize() != 1) {
            my_queue.enqueue(my_queue.dequeue());
            counter++;
            if (counter == k - 1) {
                my_queue.dequeue();
                counter = 0;
            }
        }

        // Print result in file
        wr.print(my_queue.getFirst());

        // Close file
        wr.close();
    }
}
